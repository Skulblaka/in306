package ch.rootlogin.hftm.blog.controller;

import java.util.Optional;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.inject.Named;

import ch.rootlogin.hftm.blog.models.Post;
import ch.rootlogin.hftm.blog.models.repo.PostRepository;

@Named
@RequestScoped
public class PostController extends BaseController
{
	public static final String POSTS_PAGE = "index.xhtml";
	public static final String POST_PAGE = "post.xhtml";

	@Inject
	private PostRepository repo;

	private Optional<Post> currentPost;

	public String getPostId()
	{
		final Optional<String> idOrNothing = getParameter("id");
		return idOrNothing.isPresent()
				? idOrNothing.get()
				: "";
	}

	public String getPostsPage()
	{
		return POSTS_PAGE;
	}

	public String getPostPage()
	{
		return POST_PAGE;
	}

	public Iterable<Post> getPosts()
	{
		return repo.findAll();
	}

	public Optional<Long> parsePostId()
	{
		try
		{
			return Optional.of(Long.parseLong(getPostId()));
		}
		catch (final NumberFormatException nfe)
		{
			addMessage(FacesMessage.SEVERITY_ERROR, "Falsche Post ID.");
			return Optional.empty();
		}
	}

	public String checkWhetherPostExists()
	{
		findCurrentPost();
		if (!currentPost.isPresent())
		{
			addMessage(FacesMessage.SEVERITY_ERROR, "Post nicht gefunden.");
			return getPostsPage();
		}
		return null;
	}

	private void findCurrentPost()
	{
		if (currentPost != null)
		{
			return;
		}
		currentPost = Optional.empty();
		final Optional<Long> postIdOrNothing = parsePostId();
		if (postIdOrNothing.isPresent())
		{
			currentPost = repo.find(postIdOrNothing.get());
		}
	}

	public Post getCurrentPost()
	{
		findCurrentPost();
		if (currentPost == null || !currentPost.isPresent())
		{
			return new Post("Post wurde nicht gefunden", "Kein Inhalt");
		}
		return currentPost.get();
	}
}
