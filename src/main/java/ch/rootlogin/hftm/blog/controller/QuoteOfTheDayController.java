package ch.rootlogin.hftm.blog.controller;

import javax.inject.Named;
import java.util.Random;

@Named
public class QuoteOfTheDayController {

    public static final String[] QUOTES = {
            "Woah, voll Gängster!",
            "Ey, wie krass!",
            "JavaEE 4 Winners!",
            "Chrömli si fein.",
            "Das geid ab!"
    };

    public String say() {
        int rnd = new Random().nextInt(QUOTES.length);

        return QUOTES[rnd];
    }
}
