package ch.rootlogin.hftm.blog.models.repo;

import java.util.Optional;

import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import ch.rootlogin.hftm.blog.models.Post;

@RequestScoped
public class PostRepository {
	@PersistenceContext
	private EntityManager em;

	public Iterable<Post> findAll() {
		return em.createQuery("SELECT p FROM Post p", Post.class).getResultList();
	}

	public Optional<Post> find(final Long id) {
		return Optional.ofNullable(em.find(Post.class, id));
	}
}
