# Test-Projekt für verteilte System

# Features
 * Mini-Blogging Tool auf Basis Schulbeispiel
 * Twitter Bootstrap 4
 * Quote of the Day
 * [Docker-Hub](https://hub.docker.com/r/rootlogin/in306)
 * Auto-build via [Gitlab CI](https://gitlab.com/rootlogin/in306/blob/master/.gitlab-ci.yml)

## Konfiguration

Die Konfiguration der Anwendung findet über Environment-Variablen statt:

 * **DB_USERNAME**: Datenbank Benutzername
 * **DB_PASSWORD**: Datenbank Passwort
 * **DB_URL**: Datenbank JDBC URL (z.B. DB_URL=mysql://db-host/db-name)
 
Die Konfigurationsparameter können entweder in der "docker-compose.yml" oder manuell via "docker run -e DB_URL=mysql://db-host/db-name rootlogin/in306" übergeben werden.

## Build/Start der Applikation mit docker-compose

**Vorraussetzungen:**

 * funktionierende docker-compose Installation
 * Port 8080 und 4848 dürfen nicht belegt sein.
 
```shell script
# Von docker hub laden (optional)
docker pull rootlogin/in306

# Applikation builden (optional)
docker-compose build

# Applikation starten
docker-compose up -d

# Applikation stoppen
docker-compose stop
```

Anschliessend kann die Blogapplikation nach wenigen Sekunden unter [localhost:8080/blog](http://localhost:8080/blog/index.xhtml) aufgerufen werden.

## Tipps und Tricks

 * Zum Schnellen rebuilden und neustarten, kann das Script `./rebuild.sh` unter Linux verwendet werden-