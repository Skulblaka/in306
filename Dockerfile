FROM maven:3-jdk-8 as builder

RUN mkdir /tmp/build

COPY src/ /tmp/build/src/
COPY pom.xml /tmp/build

WORKDIR /tmp/build

RUN mvn package

FROM payara/server-full

# Mysql-Treiber
RUN wget -O $PAYARA_DIR/glassfish/domains/production/lib/database-connector.jar http://central.maven.org/maven2/mysql/mysql-connector-java/5.1.48/mysql-connector-java-5.1.48.jar

# WAR-File deployen
COPY --from=builder /tmp/build/target/blog-*.war $DEPLOY_DIR/blog.war
COPY docker-entrypoint.sh /docker-entrypoint.sh

# Modify docker entrypoint
CMD ["/docker-entrypoint.sh"]